#!/bin/sh
# source venv/bin/activate
exec gunicorn -b :5000 -w 1 --access-logfile - --error-logfile - sparkconf:app
