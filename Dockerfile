FROM python:3.8-slim-buster

RUN groupadd -r sparkconf && useradd --no-log-init -r -g sparkconf sparkconf

WORKDIR /home/sparkconf

COPY requirements.txt requirements.txt
#RUN python -m venv venv
RUN pip install -r requirements.txt --no-cache-dir
RUN pip install gunicorn --no-cache-dir

COPY app app
COPY sparkconf.py config.py start.sh ./
RUN chmod +x start.sh

ENV FLASK_APP sparkconf.py

RUN chown -R sparkconf:sparkconf ./
USER sparkconf

EXPOSE 5000
CMD ["./start.sh"]