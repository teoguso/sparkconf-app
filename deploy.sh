#!/usr/bin/env bash
# A valid SSH key needs to be added to the Dokku remote for this to work
set -xe
{
    ssh dokku@${DOKKU_HOST} apps:create $DOKKU_APP_NAME
} || {
    echo "Cannot create $DOKKU_APP_NAME Dokku app. Guess it is already there?"
}
{
    git remote add dokku dokku@${DOKKU_HOST}:${DOKKU_APP_NAME}
} || {
    git remote remove dokku
    git remote add dokku dokku@${DOKKU_HOST}:${DOKKU_APP_NAME}
}
git push dokku HEAD:master
# Fix automatic port forwarding from docker
ssh dokku@${DOKKU_HOST} proxy:ports-add    ${DOKKU_APP_NAME} http:80:5000
ssh dokku@${DOKKU_HOST} proxy:ports-remove ${DOKKU_APP_NAME} http:5000:5000

# Check status
ssh dokku@${DOKKU_HOST} apps:report
