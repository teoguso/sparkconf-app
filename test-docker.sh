#!/usr/bin/env bash
IMAGE_NAME="sparkconf-test"
docker build -t $IMAGE_NAME .
docker run --rm --name test-docker-local -p 5000:5000 $IMAGE_NAME