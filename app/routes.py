from flask import render_template, flash, redirect, url_for
from app import app
from app.forms import SparkConfForm
from app.configurator import calc_spark_configuration


@app.route("/", methods=["GET", "POST"])
@app.route("/index", methods=["GET", "POST"])
def index():
    form = SparkConfForm()
    result = None
    if form.validate_on_submit():
        result = calc_spark_configuration(form)
    return render_template("form.html", form=form, result=result)


@app.route("/about")
def about():
    return render_template("about.html", main_header="About")
