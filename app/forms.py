from flask_wtf import FlaskForm
from wtforms import IntegerField, FloatField, BooleanField, SubmitField
from wtforms.validators import DataRequired, NumberRange


class SparkConfForm(FlaskForm):
    cluster_nodes = IntegerField(
        "Number of Nodes", validators=[DataRequired(), NumberRange(1, 2048)]
    )
    # At least two CPUs, since one is always reserved for Hadoop/Yarn
    node_cpus = IntegerField(
        "Number of CPUs per Node", validators=[DataRequired(), NumberRange(2, 1024)]
    )
    # Same here: Ram is defined as an integer, so one GB would not leave space for the heap
    node_ram = FloatField(
        "Available RAM per Node (GiB)",
        validators=[DataRequired(), NumberRange(2, 4096)],
    )
    force_fat_executors = BooleanField("Force fat executors (Spark)")
    submit = SubmitField("Calculate Configuration")
