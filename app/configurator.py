def calc_executor_cores(available_cores):
    """Calculate optimal number of cores per executor

    :param available_cores:
    :return:
    """
    executor_cores_max = 5
    if available_cores >= executor_cores_max:
        executor_cores = min(executor_cores_max, available_cores // 2)
    else:
        executor_cores = max(1, available_cores // 2)
    remainder_cores = available_cores % executor_cores
    while remainder_cores > 1 and executor_cores > 2:
        executor_cores -= 1
        remainder_cores = available_cores % executor_cores
    return executor_cores


def calc_spark_configuration(form):
    """Calculate an optimal Spark configuration based on the hardware

    :param form: a WTF form object, with expected payload as:
        - `form.cluster_nodes.data`
        - `form.node_cpus.data`
        - `form.node_ram.data`
        - `form.force_fat_executors.data`
    :return: A dictionary containing the calculated settings for Spark
    """
    n_nodes = form.cluster_nodes.data
    n_cores_node = form.node_cpus.data
    node_memory = form.node_ram.data
    force_fat_executors = form.force_fat_executors.data
    # One core per node is reserved for Yarn/Hadoop daemons
    available_cores = n_cores_node - 1
    # TODO: This should require a better optimization. 5 is a placeholder
    if not force_fat_executors:
        executor_cores = calc_executor_cores(available_cores)
    else:
        executor_cores = available_cores
    # Total CPUs available for Spark in the cluster
    available_cores_total = available_cores * n_nodes
    executors_total = available_cores_total / executor_cores
    node_executors = executors_total / n_nodes
    executor_memory = node_memory / node_executors
    # 7% is the default heap overhead value in Spark
    heap_overhead_percentage = 0.07
    executor_memory = (1 - heap_overhead_percentage) * executor_memory
    sparkconf = dict(
        executor_instances=int(executors_total),
        executor_cores=int(executor_cores),
        executor_memory=int(executor_memory),
    )
    return sparkconf
