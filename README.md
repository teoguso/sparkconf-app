# Spark Configurator
## A web application

### Debug run

When in the project root folder:
```
$ FLASK_DEBUG=1 python -m flask run
```

### Install Dependencies

Create Conda environment:
```
$ conda env create -f environment-dev.yml
```

Update current Conda environment:
```
$ conda env update -f environment-dev.yml
```