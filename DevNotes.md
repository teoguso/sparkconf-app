# Developer Notes

### Useful stuff for developing the project

### Flask 

#### Buttons and reactions to them

Are these called web forms? Here's a realpython example:
https://realpython.com/flask-by-example-part-3-text-processing-with-requests-beautifulsoup-nltk/

Answer: Yes, _web forms_ are whatever takes an input
from the user.


### Deployment

#### Docker

[Here](https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-xix-deployment-on-docker-containers)
Miguel grinberg's tutorial on how to deploy a Flask
app with Docker.

[This](https://www.digitalocean.com/community/tutorials/how-to-set-up-laravel-nginx-and-mysql-with-docker-compose)
is a DO tutorial on docker-compose.

#### Dokku app framework

Dokku is (in a nutshell) an open-source version of Heroku,
with some extra flexibility added.
[This guide](http://dokku.viewdocs.io/dokku/deployment/application-deployment/)
should provide enough info for basic usage of Dokku.


##### Setting up ports on Dokku

Dokku will forward all ports that are explicitely exposed
in the Dockerfile
(see [here](http://dokku.viewdocs.io/dokku/networking/port-management/)).
This is a problem when the server only exposes port 80
for http, while apps will in general use different ports.
To fix this behaviour, the following Dokku commands need to be
run after deployment:
```
$ ssh dokku@<host-ip-address> proxy:ports-add sparkconf http:80:5000
$ ssh dokku@<host-ip-address> proxy:ports-remove sparkconf http:5000:5000
```
assuming that port 5000 is exposed by the Docker image and port
80 is the one the server allows.


##### Configure domains

This can be a tricky task.
[This](http://dokku.viewdocs.io/dokku~v0.16.4/configuration/domains/)
is an extensive description of how that should work.


#### SSH keys

This part requires several steps:
- Create and add an SSH key to Dokku (the public key) and to 
the variables in Gitlab CI (the private key, name must be
`SSH_PRIVATE_KEY`).
- Add a `before_script` section in `gitlab-ci.yml` where
  the ssh agent is made aware of your ssh key.

[This page](https://docs.gitlab.com/ee/ci/ssh_keys/)
in the Gitlab docs describes the process quite extensively.
